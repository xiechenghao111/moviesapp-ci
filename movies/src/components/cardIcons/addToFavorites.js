import React, { useContext } from "react";
import { MoviesContext } from "../../contexts/moviesContext";
import IconButton from "@mui/material/IconButton";

import PlaylistAddIcon from "@mui/icons-material/PlaylistAdd";
const AddToFavoritesIcon = ({ movie }) => {
  const context = useContext(MoviesContext);

  const handleAddToFavorites = (e) => {
    e.preventDefault();
    context.addToFavorites(movie);
  };

  return (
    <IconButton aria-label="add to favorites" onClick={handleAddToFavorites}>
     
      <PlaylistAddIcon color="primary" fontSize="large" />
      
    </IconButton>
  );
};

export default AddToFavoritesIcon;